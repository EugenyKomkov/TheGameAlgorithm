public class Ball extends CellObject {

    int number;

    enum MoveCellResult{
        Pass,
        FallToHoleOrPass,
        FallToHoleOrStopped,
        Stopped
    }

    public Ball(Type type) {
        super(type);
    }

    public Ball(Type type, int number) {
        super(type);
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public MoveCellResult move(Cell cell, Direction to){
        boolean isWall = cell.getWall(to);
        if(!cell.hasHole()){
            return isWall ? MoveCellResult.Stopped : MoveCellResult.Pass;
        } else {
            return isWall ? MoveCellResult.FallToHoleOrStopped : MoveCellResult.FallToHoleOrPass;
        }
    }

}
