import sun.awt.image.ImageWatched;

import java.util.*;
import java.util.stream.Collectors;

public class Main {

    private int size;

    private Map<Coordinate, Cell> gameField = new LinkedHashMap<>();
    private Map<Coordinate, Ball> balls = new HashMap<>();
    private Map<Integer, Coordinate> holes = new HashMap<>();
    private Map<Coordinate, PossibleMovement> movements = new HashMap<>();
    private LinkedList<LinkedList<Step>> bestSteps = new LinkedList<>();

    private Map<Coordinate, Integer> newPosition;
    private Map<Coordinate, Integer> newPositionRemovedBalls;

    public Main(int size, ArrayList<Coordinate> inputBalls, ArrayList<Coordinate> inputHoles, ArrayList<WallCoordinate> inputWalls) {
        this.size = size;
        //create board from empty cells
        for (int i = 1; i <= size; i++) {
            for (int j = 1; j <= size; j++) {
                gameField.put(new Coordinate(i, j), new Cell(CellObject.Type.BoardCell));
            }
        }
        //create perimeter walls
        for (int i = 1; i <= size; i++) {
            Coordinate down = new Coordinate(i, 1);
            Coordinate right = new Coordinate(size, i);
            Coordinate left = new Coordinate(1, i);
            Coordinate up = new Coordinate(i, size);

            gameField.get(down).setWall(Direction.South);
            gameField.get(right).setWall(Direction.East);
            gameField.get(left).setWall(Direction.West);
            gameField.get(up).setWall(Direction.North);
        }

        for (WallCoordinate wall : inputWalls) {
            if (wall.getPair1().getX() == wall.getPair2().getX()) {
                // vertical neigbours
                if (wall.getPair1().getY() < wall.getPair2().getY()) {
                    // first is upper than second
                    gameField.get(wall.getPair1()).setWall(Direction.South);
                    gameField.get(wall.getPair2()).setWall(Direction.North);
                } else {
                    gameField.get(wall.getPair1()).setWall(Direction.North);
                    gameField.get(wall.getPair2()).setWall(Direction.South);
                }
            } else {
                // horisontal neighbours
                if (wall.getPair1().getX() < wall.getPair2().getX()) {
                    // first is left to second
                    gameField.get(wall.getPair1()).setWall(Direction.East);
                    gameField.get(wall.getPair2()).setWall(Direction.West);
                } else {
                    gameField.get(wall.getPair1()).setWall(Direction.West);
                    gameField.get(wall.getPair2()).setWall(Direction.East);
                }
            }
        }

        //add balls to board
        int ballNumber = 1;
        for (Coordinate coord : inputBalls) {
            this.balls.put(coord, new Ball(CellObject.Type.Ball, ballNumber));
            ballNumber++;
        }

        //add holes to board
        int holeNumber = 1;
        for (Coordinate coord : inputHoles) {
            gameField.get(coord).setHole(holeNumber);
            this.holes.put(holeNumber, coord);
            holeNumber++;
        }
    }

    public Map<Coordinate, Cell> getField() {
        return gameField;
    }

    public int getSize() {
        return size;
    }

    public Map<Coordinate, Ball> getBalls() {
        return balls;
    }

    public Map<Integer, Coordinate> getHoles() {
        return holes;
    }

    public Map<Coordinate, PossibleMovement> getMovements() {
        return movements;
    }

    public void startAlgorithm() {
        findAllPossibleMovements();
        findAllMoves();
    }

    public void findAllMoves() {
        Map<Coordinate, Integer> ballsLocal;
        Map<Coordinate, Integer> holesLocal;

        ballsLocal = balls.entrySet().stream().collect(Collectors.toMap(c -> c.getKey(), i -> i.getValue().getNumber()));
        holesLocal = holes.entrySet().stream().collect(Collectors.toMap(c -> c.getValue(), i -> i.getKey()));

        Step startStep = new Step(ballsLocal, holesLocal);
        LinkedList<Step> steps = new LinkedList<>();
        steps.push(startStep);
        LinkedList<LinkedList<Step>> allMoves = new LinkedList<>();
        allMoves.push(steps);

        runGame(allMoves);
    }

    private void runGame(LinkedList<LinkedList<Step>> moves) {
        while (!moves.isEmpty()) {
            LinkedList<Step> currSteps = moves.getFirst();
            Map<Coordinate, Integer> positions = currSteps.getLast().getOccupiedCells();
            if (positions.size() == 0) {
                saveMoves(currSteps);
            } else {
                LinkedList<Step> north = addStep(currSteps, Direction.North);
                if (!(north != null && north.isEmpty())) {
                    moves.addLast(north);
                }
                LinkedList<Step> south = addStep(currSteps, Direction.South);
                if (!(south != null && south.isEmpty())) {
                    moves.addLast(south);
                }
                LinkedList<Step> west = addStep(currSteps, Direction.West);
                if (!(west != null && west.isEmpty())) {
                    moves.addLast(west);
                }
                LinkedList<Step> east = addStep(currSteps, Direction.East);
                if (!(east != null && east.isEmpty())) {
                    moves.addLast(east);
                }
            }
        }
        moves.pop();
    }

    private boolean saveMoves(LinkedList<Step> currSteps) {
        if ((bestSteps.size() == 0) ||
                (bestSteps.getLast().size() == currSteps.size())) {
            bestSteps.addLast(currSteps);
            return true;
        } else if (bestSteps.getLast().size() > currSteps.size()) {
            bestSteps.clear();
            bestSteps.addLast(currSteps);
            return true;
        }
        return false;
    }

    private LinkedList<Step> addStep(LinkedList<Step> steps, Direction to) {
        Map<Coordinate, Integer> currPositions = steps.getLast().getOccupiedCells();
        if (currPositions.size() == 0) {
            saveMoves(steps);
            return null;
        }

        newPosition =  new HashMap<>();
        newPositionRemovedBalls = new HashMap<>();

        boolean game_ok = moveAllBalls(to,
                currPositions,
                steps.getLast().getHoles());
        if (!game_ok) {
            return null;
        }

        Step newStep = new Step(to, steps.getLast());

        if (!check(newStep, newPositionRemovedBalls, currPositions)) {
            return null;
        }

        if (!check(newStep, newPosition, currPositions)) {
            return null;
        }

        steps.addLast(newStep);
        return steps;
    }

    private boolean check(Step newStep, Map<Coordinate, Integer> checkPosition, Map<Coordinate, Integer> currPositions) {
        for (Map.Entry<Coordinate, Integer> ball : checkPosition.entrySet()) {
            for (Map.Entry<Coordinate, Integer> previous : currPositions.entrySet()) {
                if (ball.getValue().equals(previous.getValue())) {
                    if (!newStep.setBallPosition(ball.getValue(), ball.getKey(), previous.getKey())) {
                        return false;
                    }
                    break;
                }
            }
        }
        return true;
    }

    private boolean moveAllBalls(Direction to, Map<Coordinate, Integer> currPositions, Map<Coordinate, Integer> openHoles) {
        int begin = 0;
        int end = 0;
        boolean x_before_y = false;

        switch (to) {
            case North:
                begin = 1;
                end = size;
                x_before_y = false;
                break;
            case West:
                begin = 1;
                end = size;
                x_before_y = true;
                break;
            case South:
                begin = size;
                end = 1;
                x_before_y = false;
                break;
            case East:
                begin = size;
                end = 1;
                x_before_y = true;
                break;
        }

        for (int i = begin; i >= 1 && i <= size; i = (begin < end) ? i + 1 : i - 1) {
            for (int j = begin; j >= 1 && j <= size; j = (begin < end) ? j + 1 : j - 1) {
                Coordinate currentCell = (x_before_y) ? new Coordinate(i, j) : new Coordinate(j, i);

                Integer ballNumber;
                Integer cellNumber = currPositions.get(currentCell);
                if (cellNumber == null) {
                    continue;
                } else {
                    ballNumber = cellNumber;
                }

                Coordinate nextCell = new Coordinate(movements.get(currentCell).getNeighbour(to).getX(), movements.get(currentCell).getNeighbour(to).getY());
                ArrayList<Coordinate> holesOnWay = new ArrayList<>();
                holesOnWay.addAll(movements.get(currentCell).getHoles(to));
                boolean isHoleReached = false;
                for (Coordinate hole : holesOnWay) {
                    //is gap open?
                    if (openHoles.containsKey(hole)) {
                        if (openHoles.get(hole).equals(ballNumber)) {
                            nextCell = hole;
                            isHoleReached = true;
                            openHoles.remove(hole);
                            break;
                        } else {
                            // Game over
                            return false;
                        }
                    }
                }

                // is cell empty?
                if (!isHoleReached) {
                    // cell can be occupied
                    Coordinate destination = new Coordinate(nextCell.getX(), nextCell.getY());
                    while ((newPosition != null) && (newPosition.get(destination) != null) &&
                            (destination != getNextCell(currentCell, ReverseDirection(to)))) {
                        destination = getNextCell(destination, ReverseDirection(to));
                    }
                    nextCell = destination;

                }

                // set new ball position
                if (isHoleReached) {
                    newPositionRemovedBalls.put(nextCell, ballNumber);
                } else {
                    newPosition.put(nextCell, ballNumber);
                }
            }
        }
        return true;
    }

    private void findAllPossibleMovements() {
        for (Coordinate key : gameField.keySet()) {

            PossibleMovement moves = new PossibleMovement();
            moves = findPossibleMovesInDirection(moves, key, Direction.North);
            moves = findPossibleMovesInDirection(moves, key, Direction.West);
            moves = findPossibleMovesInDirection(moves, key, Direction.South);
            moves = findPossibleMovesInDirection(moves, key, Direction.East);

            movements.put(key, moves);
        }
    }

    private PossibleMovement findPossibleMovesInDirection(PossibleMovement moves, Coordinate currentCell, Direction to) {

        Ball.MoveCellResult result = Ball.MoveCellResult.Pass;

        Coordinate startCell = new Coordinate(currentCell.getX(), currentCell.getY());
        Coordinate stopCell;

        while (result != Ball.MoveCellResult.Stopped) {
            Entry<Ball.MoveCellResult, Coordinate> tiltResult = tiltGameFieldToDirection(startCell, to);
            result = tiltResult.getLeft();
            stopCell = tiltResult.getRight();

            switch (result) {
                case Pass:
                    break;
                case Stopped:
                    moves.addNeighbour(to, stopCell);
                    break;
                case FallToHoleOrPass:
                    moves.addHole(to, stopCell);
                    stopCell = getNextCell(stopCell, to);
                    break;
                case FallToHoleOrStopped:
                    result = Ball.MoveCellResult.Stopped;
                    moves.addNeighbour(to, stopCell);
                    moves.addHole(to, stopCell);
                    break;
            }
            startCell = stopCell;
        }
        return moves;
    }

    private Entry<Ball.MoveCellResult, Coordinate> tiltGameFieldToDirection(Coordinate startCell, Direction to) {
        //create "virtual" ball
        Ball ball = new Ball(CellObject.Type.Ball);

        Ball.MoveCellResult result = Ball.MoveCellResult.Pass;
        Coordinate currentCell = new Coordinate(startCell.getX(), startCell.getY());
        Coordinate nextCell = currentCell;

        while (result == Ball.MoveCellResult.Pass) {
            currentCell = nextCell;
            Cell newCell = gameField.get(currentCell);
            result = ball.move(newCell, to);
            switch (result) {
                case FallToHoleOrPass:
                    if (currentCell.equals(startCell)) {
                        result = Ball.MoveCellResult.Pass;
                    }
                    break;
                case FallToHoleOrStopped:
                    if (currentCell.equals(startCell)) {
                        result = Ball.MoveCellResult.Stopped;
                    }
                    break;
                case Stopped:
                case Pass:
                    //do nothing
                    break;
            }
            nextCell = getNextCell(currentCell, to);
        }
        return new Entry<>(result, currentCell);
    }

    private Coordinate getNextCell(Coordinate currentCell, Direction to) {
        Coordinate cell = currentCell;
        switch (to) {
            case North:
                int y1 = currentCell.getY();
                if (y1 + 1 > getSize())
                    y1 = getSize() - 1;
                cell.setY(y1 + 1);
                break;
            case West:
                int x1 = currentCell.getX();
                if (x1 + 1 > getSize())
                    x1 = getSize();
                cell.setX(x1 - 1);
                break;
            case South:
                int y2 = currentCell.getY();
                if (y2 + 1 > getSize())
                    y2 = getSize();
                cell.setY(y2 - 1);
                break;
            case East:
                int x2 = currentCell.getX();
                if (x2 + 1 > getSize())
                    x2 = getSize() - 1;
                cell.setX(x2 + 1);
                break;
        }
        return cell;
    }

    private Direction ReverseDirection(Direction d) {
        Direction reverse = null;
        switch (d) {
            case North:
                reverse = Direction.South;
                break;
            case West:
                reverse = Direction.East;
                break;
            case South:
                reverse = Direction.North;
                break;
            case East:
                reverse = Direction.West;
                break;
        }
        return reverse;
    }

    public void printSteps() {

        for (LinkedList<Step> stepList : bestSteps) {
            for (Step step : stepList) {
                if (!step.isStart()) {
                    System.out.println(step.getStep());
                }
            }
            System.out.println("\n");
        }
    }

    public static void main(String[] args) {
        //4 2 2 - size, balls = holes, walls
        // 1 1 2 3  - balls (0,0),(1,2)
        // 1 4 4 2  - holes (0,3),(3,1)
        // 1 2 1 3 3 3 4 3 - walls ((0,1),(0,2)),((2,2),(3,2))
        ArrayList<Coordinate> inputBalls = new ArrayList<>();
        inputBalls.add(new Coordinate(1, 1));
        inputBalls.add(new Coordinate(2, 3));
        ArrayList<Coordinate> inputHoles = new ArrayList<>();
        inputHoles.add(new Coordinate(1, 4));
        inputHoles.add(new Coordinate(4, 2));
        ArrayList<WallCoordinate> inputWalls = new ArrayList<>();
        inputWalls.add(new WallCoordinate(1, 2, 1, 3));
        inputWalls.add(new WallCoordinate(3, 3, 4, 3));
        Main gameBoard = new Main(4, inputBalls, inputHoles, inputWalls);
        gameBoard.startAlgorithm();
        gameBoard.printSteps();

    }
}
