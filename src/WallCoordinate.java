public class WallCoordinate {
    private Coordinate pair1;
    private Coordinate pair2;

    public WallCoordinate(int x1, int y1, int x2, int y2) {
        this.pair1 = new Coordinate(x1, y1);
        this.pair2 = new Coordinate(x2, y2);
    }

    public WallCoordinate(Coordinate pair1, Coordinate pair2) {
        this.pair1 = pair1;
        this.pair2 = pair2;
    }

    public Coordinate getPair1() {
        return pair1;
    }

    public Coordinate getPair2() {
        return pair2;
    }
}
