import java.util.Map;
import java.util.Optional;

public class Step {
    Direction step;
    boolean isStart;
    Map<Coordinate, Integer> occupiedCells;
    Map<Coordinate, Integer> holes;

    public Step(Direction step, Step previousStep) {
        this.step = step;
        this.isStart = false;
        this.occupiedCells = previousStep.getOccupiedCells();
        this.holes = previousStep.getHoles();
    }

    public Step(Map<Coordinate, Integer> occupiedCells, Map<Coordinate, Integer> holes) {
        this.occupiedCells = occupiedCells;
        this.holes = holes;
    }

    public Direction getStep() {
        return step;
    }

    public Map<Coordinate, Integer> getOccupiedCells() {
        return occupiedCells;
    }

    public void setOccupiedCells(Map<Coordinate, Integer> occupiedCells) {
        this.occupiedCells = occupiedCells;
    }

    public Map<Coordinate, Integer> getHoles() {
        return holes;
    }

    public void setHoles(Map<Coordinate, Integer> holes) {
        this.holes = holes;
    }

    public boolean isStart() {
        return isStart;
    }

    public void setStart(boolean start) {
        isStart = start;
    }

    public Coordinate getBallPosition(Integer ballNumber) {
        Coordinate result = occupiedCells.entrySet().stream().filter(map -> map.getValue().equals(ballNumber)).findFirst().get().getKey();
        if (result != null) {
            return result;
        }
        return new Coordinate(0, 0);
    }

    public boolean setBallPosition(Integer ballNumber, Coordinate currentCell, Coordinate previousCell) {
        Coordinate resultCell = occupiedCells.entrySet().stream().filter(map -> map.getKey().equals(previousCell)).findFirst().orElse(null).getKey();
        if(resultCell != null){
            occupiedCells.remove(resultCell);
        }
        //resultCell =
        Map.Entry<Coordinate, Integer> entry = occupiedCells.entrySet().stream().filter(map -> map.getKey().equals(currentCell)).findFirst().orElse(null);
        if (entry != null) {
            //cell is occupied
            return false;
        } else {
            //check if we have hole under the ball
            //Coordinate hole =
            Map.Entry<Coordinate, Integer> hole = holes.entrySet().stream().filter(map -> map.getKey().equals(currentCell)).findFirst().orElse(null);
            if (hole != null) {
                Integer holeNumber = holes.entrySet().stream().filter(map -> map.getKey().equals(currentCell)).findFirst().get().getValue();
                if(holeNumber.equals(ballNumber)){
                    holes.remove(hole);
                    return true;
                }
            } else {
                occupiedCells.put(currentCell,ballNumber);
                return true;
            }
        }
        return false;
    }
}
