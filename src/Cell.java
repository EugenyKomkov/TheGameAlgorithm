import java.util.Objects;

public class Cell extends CellObject {

    public class Walls {
        boolean north;
        boolean south;
        boolean west;
        boolean east;
    }

    private Walls wall = new Walls();
    private boolean hasHole;
    private int ballNumber;

    //empty cell
    public Cell(Type type) {
        super(type);
    }

    //cell with hole for ballNumber
    public Cell(Type type, int ballNumber) {
        super(type);
        this.ballNumber = ballNumber;
        this.hasHole = true;
    }

    //cell with walls
    public Cell(Type type, Walls wall) {
        super(type);
        this.wall = wall;
    }

    public boolean hasHole() {
        return hasHole;
    }

    public int getBallNumber() {
        return ballNumber;
    }

    public void setHole(int ballNumber) {
        this.ballNumber = ballNumber;
        this.hasHole = true;
    }

    public boolean getWall(Direction at) {
        switch (at){
            case North:
                return this.wall.north;
            case South:
                return this.wall.south;
            case West:
                return this.wall.west;
            case East:
                return this.wall.east;
        }
        return false;
    }

    public void setWall(Direction at) {
        switch (at){
            case North:
                this.wall.north = true;
                break;
            case South:
                this.wall.south = true;
                break;
            case West:
                this.wall.west = true;
                break;
            case East:
                this.wall.east = true;
                break;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cell cell = (Cell) o;

        return ballNumber == cell.ballNumber;
    }

    @Override
    public int hashCode() {
        return ballNumber;
    }
}
