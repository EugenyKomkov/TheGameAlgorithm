public class CellObject {
    enum Type{
        BoardCell,
        Ball
    }
    Type type;

    public CellObject(Type type) {
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
