import java.util.ArrayList;

public class PossibleMovement {

    private Coordinate neighbourNorth;
    private Coordinate neighbourSouth;
    private Coordinate neighbourWest;
    private Coordinate neighbourEast;

    private ArrayList<Coordinate> holesNorth = new ArrayList<>();
    private ArrayList<Coordinate> holesSouth = new ArrayList<>();
    private ArrayList<Coordinate> holesWest = new ArrayList<>();
    private ArrayList<Coordinate> holesEast = new ArrayList<>();

    public Coordinate getNeighbour(Direction to) {
        switch (to) {
            case North:
                return neighbourNorth;
            case South:
                return neighbourSouth;
            case West:
                return neighbourWest;
            case East:
                return neighbourEast;
        }
        return null;
    }

    public void addNeighbour(Direction at, Coordinate cell) {
        switch (at) {
            case North:
                this.neighbourNorth = cell;
                break;
            case South:
                this.neighbourSouth = cell;
                break;
            case West:
                this.neighbourWest = cell;
                break;
            case East:
                this.neighbourEast = cell;
                break;
        }
    }

    public ArrayList<Coordinate> getHoles(Direction to) {
        switch (to) {
            case North:
                return holesNorth;
            case South:
                return holesSouth;
            case West:
                return holesWest;
            case East:
                return holesEast;
        }
        return null;
    }

    public void addHole(Direction at, Coordinate cell) {
        switch (at) {
            case North:
                this.holesNorth.add(cell);
                break;
            case South:
                this.holesSouth.add(cell);
                break;
            case West:
                this.holesWest.add(cell);
                break;
            case East:
                this.holesEast.add(cell);
                break;
        }
    }
}
