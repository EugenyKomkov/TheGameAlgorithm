//help class to store 2 different values
public class Entry<L, R> {

    private final L left;
    private final R right;

    public Entry(L left, R right) {
        this.left = left;
        this.right = right;
    }

    public L getLeft() {
        return left;
    }

    public R getRight() {
        return right;
    }
}
