public class Coordinate {
    private int x;
    private int y;

    public Coordinate() {
        this.x = 1;
        this.y = 1;
    }

    public Coordinate(int x, int y) {
        this.x = x > 0 ? x : 1;
        this.y = y > 0 ? y : 1;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x > 0 ? x : 1;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y > 0 ? y : 1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Coordinate that = (Coordinate) o;

        if (x != that.x) return false;
        return y == that.y;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }
}
